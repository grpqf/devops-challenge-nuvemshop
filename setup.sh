#!/bin/bash

yum update -y

if [ ! -e "/usr/bin/unzip" ]; then
   yum install unzip -y
fi

if  [ ! -e "/usr/bin/terraform" ]; then
    unzip terraform_0.12.18_linux_amd64.zip -d /usr/bin
fi

export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
export AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION

cd terraform
terraform init
terraform $STATE -auto-approve
