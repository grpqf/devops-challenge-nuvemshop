variable "aws_region" {
    type    = string
    default = "us-east-1" 
    #default = "us-west-1"
}

variable "ami_id" {
    type = string
    default = "ami-0083662ba17882949" #us-east-1
    #default = "ami-029800930881e1717" #us-west-1
}

variable "vpc_cidr" {
    type = string
    default = "180.10.0.0/16"
}

variable "private_subnet_1" {
    type = string 
    default = "180.10.0.0/20"
}

variable "private_subnet_2" {
    type = string
    default = "180.10.64.0/20"
}

variable "public_subnet_1" {
    type = string
    default = "180.10.16.0/20"
}

variable "public_subnet_2" {
    type = string
    default = "180.10.32.0/20"
}