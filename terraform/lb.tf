#security group
resource "aws_security_group" "sg_vpc_alb_eduardodicarte" {
  name        = "sg_vpc_alb_eduardodicarte"
  description = "Allow inbound outbound traffic"
  vpc_id      = aws_vpc.vpc_challenge_eduardodicarte.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

#====================================================================================================
#Load Balancer

resource "aws_lb" "lb_eduardodicarte" {
  name               = "alb-eduardodicarte"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.sg_vpc_alb_eduardodicarte.id}"]
  subnets            = ["${aws_subnet.subnet1_alb.id}", "${aws_subnet.subnet2_alb.id}"]

  tags = {
    Author = "Eduardo Dicarte"
  }
}

#======================================================================================================
#Target Group

resource "aws_lb_target_group" "target_group_lb_eduardodicarte" {
  name     = "tg-lb-eduardodicarte"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc_challenge_eduardodicarte.id

  health_check {
    healthy_threshold   = 5
    unhealthy_threshold = 10
    timeout             = 20
    interval            = 25
    path                = "/"
    port                = "80"
    protocol            = "HTTP"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener" "listener-443" {
  load_balancer_arn = aws_lb.lb_eduardodicarte.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group_lb_eduardodicarte.arn
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_target_group_attachment" "tg_instance_nginx_a" {
  target_group_arn = aws_lb_target_group.target_group_lb_eduardodicarte.arn
  target_id        = aws_instance.nginx_a.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "tg_instance_nginx_b" {
  target_group_arn = aws_lb_target_group.target_group_lb_eduardodicarte.arn
  target_id        = aws_instance.nginx_b.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "tg_instance_apache_a" {
  target_group_arn = aws_lb_target_group.target_group_lb_eduardodicarte.arn
  target_id        = aws_instance.apache_a.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "tg_instance_apache_b" {
  target_group_arn = aws_lb_target_group.target_group_lb_eduardodicarte.arn
  target_id        = aws_instance.apache_b.id
  port             = 80
}