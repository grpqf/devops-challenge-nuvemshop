resource "aws_vpc" "vpc_challenge_eduardodicarte" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "VPC Challenge Eduardo Dicarte"
  }
}

output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.vpc_challenge_eduardodicarte.id
}

#====================================================================================================
resource "aws_main_route_table_association" "main_route_table_vpc_eduardodicarte" {
  vpc_id         = aws_vpc.vpc_challenge_eduardodicarte.id
  route_table_id = aws_route_table.rt_nat_gw1.id
}