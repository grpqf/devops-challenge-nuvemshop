terraform {
  backend "s3" {
    bucket = "terraform-aws-eduardodicarte"
    encrypt = true
    key    = "terraform-aws-eduardodicarte/states"
  }
}
