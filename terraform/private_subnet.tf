resource "aws_subnet" "subnet_ec2_eduardodicarte_a" {
  vpc_id            = aws_vpc.vpc_challenge_eduardodicarte.id
  cidr_block        = var.private_subnet_1
  availability_zone = "${var.aws_region}a"

  tags = {
    Name = "Subnet para EC2"
    Author = "Eduardo Dicarte"
  }
}

resource "aws_subnet" "subnet_ec2_eduardodicarte_b" {
  vpc_id            = aws_vpc.vpc_challenge_eduardodicarte.id
  cidr_block        = var.private_subnet_2
  availability_zone = "${var.aws_region}b"

  tags = {
    Name = "Subnet para EC2"
    Author = "Eduardo Dicarte"
  }
}

resource "aws_route_table_association" "rt_association_ps_pb1" {
   subnet_id      = aws_subnet.subnet_ec2_eduardodicarte_a.id
   route_table_id = aws_route_table.rt_nat_gw1.id
}

resource "aws_route_table_association" "rt_association_ps_pb2" {
   subnet_id      = aws_subnet.subnet_ec2_eduardodicarte_b.id
   route_table_id = aws_route_table.rt_nat_gw2.id
}