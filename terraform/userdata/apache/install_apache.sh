#!/bin/bash

#ativando execucao do sudo sem tty
sudo sed -i -e '/Defaults    requiretty/{ s/.*/# Defaults    requiretty/ }' /etc/sudoers

#so
sudo yum update -y
sudo yum install epel-release -y

#apache
sudo yum install httpd -y

sudo cp /usr/share/httpd/noindex/index.html /var/www/html/index.html
echo "" > /etc/httpd/conf.d/welcome.conf

cat <<EOT >> /etc/httpd/conf/httpd.conf
<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>
EOT

sudo systemctl start httpd
sudo systemctl enable httpd

#firewall
sudo iptables -F
