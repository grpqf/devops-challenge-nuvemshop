#!/bin/bash

#ativando execucao do sudo sem tty
sudo sed -i -e '/Defaults    requiretty/{ s/.*/# Defaults    requiretty/ }' /etc/sudoers

#so
yum update -y
sudo yum install epel-release -y

#nginx
sudo yum install nginx -y
sudo systemctl start nginx.service
sudo systemctl enable nginx.service

#firewall
iptables -F
