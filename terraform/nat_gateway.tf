resource "aws_eip" "eip_subnet1" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gw_subnet1" {
  allocation_id = aws_eip.eip_subnet1.id
  subnet_id     = aws_subnet.subnet1_alb.id
}

resource "aws_route_table" "rt_nat_gw1" {
  vpc_id = aws_vpc.vpc_challenge_eduardodicarte.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw_subnet1.id
  }

  tags = {
    Name = "Route table Nat Gateway 1"
    Author = "Eduardo Dicarte"
  }
}

########

resource "aws_eip" "eip_subnet2" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gw_subnet2" {
  allocation_id = aws_eip.eip_subnet2.id
  subnet_id     = aws_subnet.subnet2_alb.id
}

resource "aws_route_table" "rt_nat_gw2" {
  vpc_id = aws_vpc.vpc_challenge_eduardodicarte.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw_subnet2.id
  }

  tags = {
    Name = "Route table Nat Gateway 2"
    Author = "Eduardo Dicarte"
  }
}