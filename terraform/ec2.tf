#Security Group

resource "aws_security_group" "sg_ec2_eduardodicarte" {
  name        = "sg_ec2_eduardodicarte"
  description = "Allow inbound outbound traffic"
  vpc_id      = aws_vpc.vpc_challenge_eduardodicarte.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}
#==============================================================================================
#EC2

#nginx 

resource "aws_instance" "nginx_a" {
  ami                         = var.ami_id
  instance_type               = "t2.micro"
  user_data                   = file("userdata/nginx/install_nginx.sh")
  availability_zone           = "${var.aws_region}a"
  subnet_id                   = aws_subnet.subnet_ec2_eduardodicarte_a.id
  vpc_security_group_ids      = [aws_security_group.sg_ec2_eduardodicarte.id]

  tags = {
    Name = "Nginx"
    Author = "Eduardo Dicarte"
  }
}

resource "aws_instance" "nginx_b" {
  ami                         = var.ami_id
  instance_type               = "t2.micro"
  user_data                   = file("userdata/nginx/install_nginx.sh")
  availability_zone           = "${var.aws_region}b"
  subnet_id                   = aws_subnet.subnet_ec2_eduardodicarte_b.id
  vpc_security_group_ids      = [aws_security_group.sg_ec2_eduardodicarte.id]

  tags = {
    Name = "Nginx"
    Author = "Eduardo Dicarte"
  }
}

#apache

resource "aws_instance" "apache_a" {
  ami                         = var.ami_id
  instance_type               = "t2.micro"
  user_data                   = file("userdata/apache/install_apache.sh")
  availability_zone           = "${var.aws_region}a"
  subnet_id                   = aws_subnet.subnet_ec2_eduardodicarte_a.id
  vpc_security_group_ids      = [aws_security_group.sg_ec2_eduardodicarte.id]

  tags = {
    Name = "Apache"
    Author = "Eduardo Dicarte"
  }
}

resource "aws_instance" "apache_b" {
  ami                         = var.ami_id
  instance_type               = "t2.micro"
  user_data                   = file("userdata/apache/install_apache.sh")
  availability_zone           = "${var.aws_region}b"
  subnet_id                   = aws_subnet.subnet_ec2_eduardodicarte_b.id
  vpc_security_group_ids      = [aws_security_group.sg_ec2_eduardodicarte.id]

  tags = {
    Name = "Apache"
    Author = "Eduardo Dicarte"
  }
}