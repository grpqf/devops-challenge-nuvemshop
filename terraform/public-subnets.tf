resource "aws_subnet" "subnet1_alb" {
    vpc_id = aws_vpc.vpc_challenge_eduardodicarte.id
    map_public_ip_on_launch = true
    availability_zone = "${var.aws_region}a"

    cidr_block = var.public_subnet_1

    tags = {
       Name = "Subnet 1 para Multi zona do ALB"
       Author = "Eduardo Dicarte"
    }
}

resource "aws_route_table_association" "rt_association_sn1" {
   subnet_id      = aws_subnet.subnet1_alb.id
   route_table_id = aws_route_table.rt_internet_gw.id
}

resource "aws_subnet" "subnet2_alb" {
    vpc_id = aws_vpc.vpc_challenge_eduardodicarte.id
    availability_zone = "${var.aws_region}b"
    map_public_ip_on_launch = true

    cidr_block = var.public_subnet_2

    tags = {
        Name = "Subnet 2 para Multizona do ALB"
        Author = "Eduardo Dicarte"
    }
}

resource "aws_route_table_association" "rt_association_sn2" {
   subnet_id      = aws_subnet.subnet2_alb.id
   route_table_id = aws_route_table.rt_internet_gw.id
}