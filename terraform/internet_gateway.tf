resource "aws_internet_gateway" "internet_gw" {
  vpc_id = aws_vpc.vpc_challenge_eduardodicarte.id

  tags = {
    Name = "Internet Gateway"
    Author = "Eduardo Dicarte"
  }
}

resource "aws_route_table" "rt_internet_gw" {
  vpc_id = aws_vpc.vpc_challenge_eduardodicarte.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gw.id
  }

  tags = {
    Name = "Route table Internet Gateway Eduardo Dicarte"
  }
}
