# devops-challenge-nuvemshop

# Introdução 
O projeto foi desenvolvido utilizando exclusivamente o terraform para provisionamento na AWS, e shell script para instalação dos servidores (apache e nginx)

Os testes foram feitos em 2 regiões distintas, us-east-1 e us-west-1

Para fins de automação e infraestrutura escalável e orientada a código, com exceção dos buckets e de permissões de acesso, as configurações de rede 
serão criadas do zero.

Como dito acima, é necessário criar as permissões de acesso via IAM e criar o bucket na região que será testada. O nome do bucket pode ser encontrado 
e alterado no arquivo backend.tf

A solução foi implementada tomando por referência a região us-east-1, para que a portabilidade para a região us-west-1 fosse feita, foi validado algumas AMIs,
isso ocorre porque alguns builds do CentOS podem exigir alguns comportamentos específicos, é recomendado testes contínuos com o provisionamento agendado, 
para que seja possível antecipar problemas que possam aparecer na atualização de pacotes.

# Variáveis

As variaveis foram dividas em variaveis do CI, configuradas pelo gitlab e variáveis do terraform configuradas no arquivo "variables.tf".

## CI
As variaveis do CI são para fins de autenticação e mudança de estado. São detalhadas abaixo.

* AWS_ACCESS_KEY_ID -> Define o ID para a conta AWS
* AWS_SECRET_ACCESS_KEY -> Define o SECRET para a conta AWS
* AWS_DEFAULT_REGION -> Define a região que a solução irá provisionar 
* STATE -> Define o estado que será considerado no provisionamento (apply ou destroy)

## Terraform
As variaveis do terraform s~ão para configuração de vpc, subnets e ami. São detalhadas abaixo.

* aws_region -> Define a região para as instancias
* ami_id -> ID para AMI
* vpc_cidr -> CIDR para a VPC
* private_subnet_1 -> CIDR para a subnet privada na zona 1 (zona a)
* private_subnet_2 -> CIDR para a subnet privada na zona 2 (zona b)
* public_subnet_1 -> CIDR para a subnet publica na zona 1 (zona a)
* public_subnet_2 -> CIDR para a subnet publica na zona 2 (zona b)

## Sugestões de melhoria

* Utilização do ansible para gerenciar o terraform, dessa forma o terraform pode ser convertido em role e um mesmo c´ódigo pode ser utilizado para todos 
* os provisionamento.
* Utilização do ansible para gerenciar a configuração das instancias (instalação do nginx e apache) 
* Testes contíínuos no provisionamento, instalação e validação das soluções
* Utilização do CI para empurrar configuração dentro da instancia, via ansible poderia ser instalado o agent do gitlab, e atualizações podem ser feitas (gerencia de configuração)
* 


>  # **Observações**
### Os testes foram feitos nas regiões us-east-1 e us-west-1, para que a solução seja aplicado as demais regiões é preciso validar as AMIs, ou copiar as AMIs para a região que se deseja. É altamente recomendado que testes contínuos sejam aplicados nas regiões que a empresa mantém a infraestrutura. Também é recomendado que AMIs especificas sejam mantidas pela empresa, tanto para fins de segurança como para fins de integridade.



